<?=$header?>


<?php if(!empty($logo)) echo $logo; ?>

<?php
//show menu if any
if(isset($menu) && !empty($menu)) echo $menu; ?>

<?php
//show message if any
if (isset($message) && !empty($message)){
	echo "<div class='container my-container'>";
	echo "<div class='alert alert-danger'>".$message."</div>";
	echo "</div>";
}
?>

<? if(!empty($content)): ?>

<?=$content ;?>
<? endif; ?>

<?php if(!empty($footer)) echo $footer?>
