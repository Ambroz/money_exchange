<!DOCTYPE html>
<html lang="pl">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
		<?php echo (isset($pageTitle)) ? $pageTitle : "Money Exchanger"; ?>
	</title>

	<link rel="stylesheet" href="<?=base_url()?>assets/uikit/css/uikit.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/formbuilder/form-builder.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/formbuilder/form-render.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/custom.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/rize-builder.css">
	<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

	<link rel="icon" href="<?=base_url()?>/assets/img/favicon.ico" type="image/ico">

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>

	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>

	<script src="//cdn.jsdelivr.net/webshim/1.16/polyfiller.js"></script>
	<script>
	webshims.setOptions('forms-ext', {types: 'date'});
	webshims.polyfill('forms forms-ext');
	</script>


	<script src="<?=base_url()?>assets/uikit/js/uikit.min.js"></script>
	<script src="<?=base_url()?>assets/formbuilder/form-builder.min.js"></script>
	<script src="<?=base_url()?>assets/formbuilder/form-render.min.js"></script>
	<script src="<?=base_url()?>assets/js/custom.js"></script>
	<script src="<?=base_url()?>assets/js/rize-builder.js"></script>
	<script src="<?=base_url()?>assets/js/paypal-button.min.js"></script>

	<!-- tiny MCE -->
	<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"></script>
	<script type="text/javascript">
	tinymce.init({
		selector: "#mytextarea",
		plugins: "code paste",
		verify_html: false,
		convert_urls: false,
		paste_as_text: true
	});
	</script>


</head>
<body class="home">
