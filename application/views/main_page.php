<div class="container">
	<div class="row">
    <div class="col">

				<?php
				echo form_open(base_url().'main/get_money', 'class="form-group" id="myform"');

				echo form_input(array('id' => 'begin_date', 'class' => 'form-control', 'name' => 'begin_date', 'type' => 'date', 'placeholder' => 'Begin Date'));
				echo '</br>';
				echo form_input(array('id' => 'end_date', 'class' => 'form-control', 'name' => 'end_date', 'type' => 'date', 'placeholder' => 'End Date'));
				echo '</br>';
				echo form_submit(array('id' => 'show_submit', 'value' => 'Show','class'=>'btn btn-primary'));
				echo '</p>';
				echo form_close();

				?>


				<?php
				if(isset($info)){

					echo "<p>Zakres dat: ".$begin." - ".$end."</p>";
					?>
					<table class="table" style="width:50%;">
						<tr>
							<th>Data</th>
							<th>Kupno</th>
							<th>Sprzedaż</th>
							<th>Różnica</th>
						</tr>
						<?php

						foreach($info as $key => $value){
							$reszta = $value->ask - $value->bid;
							?>

							<tr>
								<td style="text-align:center;"><?=$value->effectiveDate?></td>
								<td style="text-align:center;"><?=$value->bid?></td>
								<td style="text-align:center;"><?=$value->ask?></td>
								<td style="text-align:center;"><?=substr($reszta, 0, 6)?></td>
							</tr>

							<?php
						}
						?>
					</table>
					<?php
				}
				?>

		</div>
	</div>
</div>
