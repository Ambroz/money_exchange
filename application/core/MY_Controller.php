<?php
class GE_Controller extends CI_Controller
{
	protected $_viewData = [];
	protected $loginData = '';

	/**
	* ste footer and header
	* @param none
	* @return constructor
	**/
	function __construct()
	{
		parent::__construct();

		//load header
		$this->_viewData['message'] = $this->session->flashdata('message');
		$this->_viewData['header'] = $this->load->view('parts/header', $this->_viewData, true);
	}
}

class MY_Controller extends GE_Controller
{
	/**
	* ste footer and header
	* @param none
	* @return constructor
	**/
	function __construct()
	{
		parent::__construct();

		$uri = $_SERVER['REQUEST_URI'];
		$s = $this->session->get_userdata();

		$ok = isset($s['user']);

		if($ok)
		{
			$this->loginData = $s['user'];
			$this->_viewData['loginData'] = $this->loginData;
		}

	}
}
