<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller {

	/**
	* Index Page for this controller.
	*
	* Maps to the following URL
	* 		http://example.com/index.php/welcome
	*	- or -
	* 		http://example.com/index.php/welcome/index
	*	- or -
	* Since this controller is set as the default controller in
	* config/routes.php, it's displayed at http://example.com/
	*
	* So any other public methods not prefixed with an underscore will
	* map to /index.php/welcome/<method_name>
	* @see https://codeigniter.com/user_guide/general/urls.html
	*/
	public function index(){

		$this->_viewData['content'] = $this->load->view('main_page', $this->_viewData, true);
		$this->load->view('parts/template', $this->_viewData);
	}

	public function get_money(){
		$begin = $this->input->post('begin_date');
		$end = $this->input->post('end_date');

		$date_begin = date_create($begin);
		$date_end = date_create($end);

		$diff=date_diff($date_begin,$date_end);

		$num_of_days = $diff->format("%a").'<br/>';

		if($begin > date('Y-m-d')){
			$this->session->set_flashdata('message', "Data początkowa nie może być większa niż obecny dzień");
			redirect('');
		}
		if($num_of_days >365){
			$this->session->set_flashdata('message', "Ilość dni nie może przekroczyć 365");
			redirect('');
		}elseif($begin > $end){

			$url = "http://api.nbp.pl/api/exchangerates/rates/c/usd/".$end."/".$begin."/?format=json";
		}elseif($end > date('Y-m-d')){
			$this->session->set_flashdata('message', "Brak danych dla dnia ".$end);
			redirect('');
		}else{
			$url = "http://api.nbp.pl/api/exchangerates/rates/c/usd/".$begin."/".$end."/?format=json";
		}
		$this->_viewData['json'] = json_decode(file_get_contents($url));
		$json = json_decode(file_get_contents($url));
		$this->_viewData['info'] = $json->rates;

		$this->_viewData['begin'] = $begin;
		$this->_viewData['end'] = $end;
		$this->_viewData['content'] = $this->load->view('main_page', $this->_viewData, true);
		$this->load->view('parts/template', $this->_viewData);

	}
}
